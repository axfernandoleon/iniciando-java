package taller1;

import java.util.Scanner;

/**
 Mi comentario
 **/
/**
 *
 * @author fernando
 */
public class Taller1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Imprimir un mensaje
        System.out.println("Hola Mundo");
        System.out.println("Este es un mensaje en consola");
        // Las variables no se declaran ni con guio alto 
        // ni tampoco la primera letra en mayuscula
        // Enteros
        int nombreVarible;
        // Decimal
        double numeroDecimal;
        // Cadena
        String mensaje="Hola chicos, prestaran atencion...";
        // Bool
        boolean bandera;
        
        System.out.println(mensaje);
        System.out.println("-----------------------------------");
       
        int a, b, r;
        //Asiganacion de Variables
        a=10;
        b=15;
        //suma de variables
        r=a+b;
        
        System.out.println("El resutlado de la suma es: "+r+" nuevo");
        System.out.println("-----------------------------------");
        // Ingresar en consola
                
        Scanner entrada = new Scanner(System.in);
        int numero;
        //Este es un mensaje que aparece en consola
        System.out.println("Ingresa un numero: ");
        //entrada de datos por consola
        numero= entrada.nextInt();
        

        if(numero%2==0){
            
            System.out.println("Es par");
        }else{ 
            System.out.println("El numero es impar");
        }
        // Ingreso de dos o mas valores 
        // por cada valor ingresado debe existir una variable
        
        int numero1;
        int numero2;
        System.out.println("ingresa valor 1: ");
        numero1=entrada.nextInt();
        System.out.println("Ingresa valor 2: ");
        numero2=entrada.nextInt();
        
        System.out.println("Este es el valor 1: "+numero1+", y este es el valor 2: "+numero2);
    }
    
}
